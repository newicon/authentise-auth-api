<?php

declare(strict_types=1);

namespace Authentise;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;

use function add_auth;
use function json_decode;

class Api
{
    /**
     * Root domain of the API
     *
     * This is the top level domain that the api sits on.
     *
     * @var string
     */
    private string $rootDomain;

    /**
     * Authenticated guzzle http client
     *
     * @var Client
     */
    private Client $guzzle;

    /**
     * Is the rootDomain https
     *
     * Defaults to true
     *
     * @var bool
     */
    private bool $https;

    /**
     * Api secret
     *
     * Gained from authenticate method called in constructor
     *
     * @var string
     */
    private string $secret;

    /**
     * Api session uuid
     *
     * Gained from authenticate method called in constructor
     *
     * @var string
     */
    private string $uuid;

    /**
     * @var string
     */
    private string $username;

    /**
     * @var string
     */
    private string $password;

    /**
     * Client constructor.
     *
     * The $rootDomain parameter is assigned to the rootDomain property of the class. The username and password are deleted after a session cookie is generated
     *
     * @param string $username
     * @param string $password
     * @param string $rootDomain - should be in the form mydomain.com. You can ignore the https stuff
     * @param Client $client - should contain a file cookie jar
     * @param bool $https
     */
    public function __construct(
        string $username,
        string $password,
        string $rootDomain,
        Client $client,
        bool $https = true
    ) {
        $this->rootDomain = $rootDomain;
        $this->https = $https;
        $this->guzzle = $client;
        $this->username = $username;
        $this->password = $password;
        $this->uuid = '';
        $this->secret = '';
    }


    /**
     * Returns the rootDomain property
     *
     * @return string
     */
    public function rootDomain(): string
    {
        return $this->rootDomain;
    }

    private function sessionUrl(): string
    {
        return implode(
            '',
            [
                $this->https ? 'https' : 'http',
                '://data.',
                $this->rootDomain(),
                '/sessions/'
            ]
        );
    }

    private function tokenUrl(): string
    {
        return implode(
            '',
            [
                $this->https ? 'https' : 'http',
                '://users.',
                $this->rootDomain(),
                '/api-tokens/'
            ]
        );
    }

    /**
     * Request session cookie
     *
     * Gets session cookie using username and password and stores it in the clients cookie jar
     * Returns true if everything worked and false if not
     *
     * @return bool
     * @throws GuzzleException
     */
    public function getSessionCookie(): bool
    {
        $response = $this->guzzle->post(
            $this->sessionUrl(),
            [
                'username' => $this->username,
                'password' => $this->password
            ]
        );

        if ($response->getStatusCode() != 201) {
            throw new WrongStatusCodeException('expected 201, got' . $response->getStatusCode());
        }

        if (!$this->guzzle->getConfig('cookies')->getCookieByName('session') instanceof SetCookie) {
            throw new \Exception('no session cookie found');
        }

        unset($this->username);
        unset($this->password);

        return true;
    }

    public function authenticate()
    {
        $response = $this->guzzle->post(
            $this->tokenUrl(),
            [
                'form_params' => [
                    'name' => 'create-bureau'
                ]
            ]
        );

        if ($response->getStatusCode() != 201) {
            throw new WrongStatusCodeException('expected 201, got ' . $response->getStatusCode());
        }

        $body = json_decode($response->getBody()->getContents());

        $this->secret = $body->secret;
        $this->uuid = $body->uuid;
    }

    /**
     * Get authenticated client
     *
     * @return Client
     */
    public function getClient() : Client
    {
        $stack = HandlerStack::create();
        $stack->push(add_auth($this->uuid, $this->secret));

        $container = [];
        $history = Middleware::history($container);
        $stack->push($history);

        return new Client([
            'handler' => $stack,
            'cookies' => $this->guzzle->getConfig('cookies')
        ]);
    }
}