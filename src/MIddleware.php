<?php

function add_auth($uuid, $secret) {
    return function (callable $handler) use ($uuid, $secret) {
        return function (
            \Psr\Http\Message\RequestInterface $request,
            array $options
        ) use ($handler, $uuid, $secret) {
            $request = $request->withHeader('Authorization', 'Basic'. base64_encode($uuid . ':' . $secret));

            return $handler($request, $options);
        };
    };
}