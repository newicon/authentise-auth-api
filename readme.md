# Authentise API Authenticate Helper 
  
  
This is a small helper class to handle authentication with the Authentise API. 

example:

    <?php 

    // 1. Set up cookie jar to hold session cookie
    
    $cookieJar = new GuzzleHttp\Cookie\FileCookieJar('./jar');
    $client = new GuzzleHttp\Client(['cookies' => $cookieJar]);
    
    try {
        $api = new Authentise\Api('username', 'password', 'example.com', $client);  
        $api->getSessionCookie();
    } catch (\Authentise\WrongStatusCodeException $exception) {
        // handle exception - a 201 response was not received
    } finally {
        // another exception - no session cookie found in the jar
    }
    
    try {
        $api->authenticate();
    } catch (\Authentise\WrongStatusCodeException $exception) {
        // handle exception
    }    
    
    
    $authentise = $authentise->getClient(); 


This `$authentise` variable is just a guzzle client instance with the session cookie and authentication header already set for each request.
