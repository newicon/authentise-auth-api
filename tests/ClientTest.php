<?php

declare(strict_types=1);

use Authentise\Api;
use Authentise\WrongStatusCodeException;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\FileCookieJar;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    function testGetSessionCookieSetsSessionCookieOnClient()
    {
        $mockHandler = new MockHandler(
            [
                new Response(
                    201, [
                    'Set-Cookie' => 'session=15e2354d-504a-49a4-ae22-26a41be33b2c; Domain=stage-auth.com; Expires=Fri, 12-Feb-2021 09:23:38 GMT; Secure;'
                ])
            ]
        );

        $handlerStack = HandlerStack::create($mockHandler);
        $cookieJar = new FileCookieJar('./jar');

        $client = new Client(['handler' => $handlerStack, 'cookies' => $cookieJar]);

        $client = new Api('username', 'password', 'stage-auth.com', $client);
        $client->getSessionCookie();

        $this->assertNotEmpty($cookieJar->getCookieByName('session'));

        unlink('./jar');
    }

    function testGetSessionCookieThrowsExceptionForNon201Code()
    {
        $this->expectException(WrongStatusCodeException::class);

        $mockHandler = new MockHandler(
            [
                new Response(200)
            ]
        );

        $handlerStack = HandlerStack::create($mockHandler);
        $client = new Client(['handler' => $handlerStack]);

        $client = new Api('username', 'password', 'stage-auth.com', $client);
        $client->getSessionCookie();
    }

    function testGetSessionCookieThrowsExceptionForMissingSessionCookie()
    {
        $this->expectException(Exception::class);

        $mockHandler = new MockHandler(
            [
                new Response(201)
            ]
        );

        $handlerStack = HandlerStack::create($mockHandler);
        $cookieJar = new FileCookieJar('./jar');

        $client = new Client(['handler' => $handlerStack, 'cookies' => $cookieJar]);

        $client = new Api('username', 'password', 'stage-auth.com', $client);
        $client->getSessionCookie();
    }

    public function testAuthenticateStoresSecretAndUUID()
    {
        $secret = (string) mt_rand(10000, 100000);
        $uuid = (string) mt_rand(10000, 100000);

        $mockHandler = new MockHandler(
            [
                new Response(201, [], json_encode([
                    'uuid' => $uuid,
                    'secret' => $secret
                ]))
            ]
        );

        $handlerStack = HandlerStack::create($mockHandler);

        $guzzle = new Client(['handler' => $handlerStack]);

        $client = new Api('username', 'password', 'example.com', $guzzle);
        $client->authenticate();

        $reflectionClass = new ReflectionClass($client);
        $reflectionUuid = $reflectionClass->getProperty('uuid');
        $reflectionSecret = $reflectionClass->getProperty('secret');

        $reflectionUuid->setAccessible(true);
        $reflectionSecret->setAccessible(true);

        $this->assertEquals($uuid, $reflectionUuid->getValue($client));
        $this->assertEquals($secret, $reflectionSecret->getValue($client));
    }

    public function testGetClientReturnsAuthenticatedGuzzleClient()
    {
        $cookieJar = new FileCookieJar('./jar');
        $cookieJar->setCookie(new \GuzzleHttp\Cookie\SetCookie([
            'session' => 'mysessioncookie'
        ]));

        $guzzle = new Client([
            'cookies' => $cookieJar
        ]);

        $client = new Api('username', 'password', 'example.com', $guzzle);

        $reflectionClass = new ReflectionClass($client);
        $reflectionUuid = $reflectionClass->getProperty('uuid');
        $reflectionSecret = $reflectionClass->getProperty('secret');
        $reflectionUuid->setAccessible(true);
        $reflectionSecret->setAccessible(true);

        $reflectionUuid->setValue($client, 'uuid');
        $reflectionSecret->setValue($client, 'secret');

        $test = $client->getClient();

        $container = [];
        $history = \GuzzleHttp\Middleware::history($container);
        $handlerStack = HandlerStack::create();
        $handlerStack->push($history);

        $reflectionClass = new ReflectionClass($test);

        $configProp = $reflectionClass->getProperty('config');
        $configProp->setAccessible(true);

        $config = $configProp->getValue($test);
        $config['handler']->push($history);

        $test->get('https://example.com');

        foreach ($container as $transaction) {
            /**
             * @var $request \GuzzleHttp\Psr7\Request
             */
            $request = $transaction['request'];

            if ($request->hasHeader('Authorization')) {
                $this->assertEquals('Basic' . base64_encode('uuid:secret'), $request->getHeader('Authorization')[0]);
                return;
            }

            $this->fail('request missing Authorization header');
        }
    }
}